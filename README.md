# The Oracle - Main API Proxy Application

NGINX Proxy Application for The Oracle Main API 

## Usage
Trigger Build - REMOVE

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)